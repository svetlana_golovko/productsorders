DROP TABLE IF EXISTS product;

CREATE TABLE product (
    id int AUTO_INCREMENT PRIMARY KEY,
    name VARCHAR(250) NOT NULL,
    price double NOT NULL,
    description VARCHAR(250) DEFAULT NULL,
    realization_date date NOT NULL
);
/*INSERT INTO product (name, price, description, realization_date) VALUES ('Piim', 0.90, 'Alma piim 1', '2022-01-01');
INSERT INTO product (name, price, description, realization_date) VALUES ('Juust', 2.2, 'Vene juust 2', '2022-01-01');
INSERT INTO product (name, price, description, realization_date) VALUES ('Vorst', 2.3, 'Laste vorst 3', '2022-01-01');*/