package com.ordersystem.ee.service;

import java.util.List;
import java.util.Optional;

import com.ordersystem.ee.persistence.model.Product;

public interface IProductService {
    Optional<Product> findById(Long id);

    Product save(Product project);

    void delete(Long id);

    List<Product> findAllAndSortBy(String fieldName, Boolean forward);

}
