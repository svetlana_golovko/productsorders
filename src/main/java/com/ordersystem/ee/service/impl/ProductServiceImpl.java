package com.ordersystem.ee.service.impl;

import java.util.List;
import java.util.Optional;

import com.ordersystem.ee.persistence.model.Product;
import com.ordersystem.ee.persistence.repository.IProductRepository;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import com.ordersystem.ee.service.IProductService;

@Service
public class ProductServiceImpl implements IProductService {

    private IProductRepository productRepository;

    public ProductServiceImpl(IProductRepository projectRepository) {
        this.productRepository = projectRepository;
    }

    @Override
    public Optional<Product> findById(Long id) {
        return productRepository.findById(id);
    }

    @Override
    public Product save(Product project) {
        return productRepository.save(project);
    }

    @Override
    public void delete(Long id) {
        productRepository.deleteById(id);
    }

    public List<Product> findAllAndSortBy(String fieldName, Boolean forward) {
        Sort sort = null;
        if (fieldName != null)
            if (forward )
               sort =  Sort.by(Sort.Order.asc(fieldName));
            else
                sort =  Sort.by(Sort.Order.desc(fieldName));
         return (List<Product>) productRepository.findAll(sort);
    };
}
