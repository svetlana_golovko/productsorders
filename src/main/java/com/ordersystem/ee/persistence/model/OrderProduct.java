package com.ordersystem.ee.persistence.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class OrderProduct {
    @Id
    @GeneratedValue
    private Long id;
    private Long orderId;
    private Long productId;
    private Double price;

}
