package com.ordersystem.ee.persistence.model;

import org.aspectj.weaver.ast.Or;

import java.time.LocalDate;


public class Order {

    private Long id;
    private LocalDate transactionDate;
    private Long clientId;


    public Order() {
    }

    public Order(LocalDate transactionDate, Long clientId) {
        this.transactionDate = transactionDate;
        this.clientId = clientId;
    }


    public Order(Order order) {
        this(order.getTransactionDate(), order.getClientId());
    }


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDate getTransactionDate() {
        return transactionDate;
    }

    public void setTransactionDate(LocalDate transactionDate) {
        this.transactionDate = transactionDate;
    }

    public Long getClientId() {
        return clientId;
    }

    public void setClientId(Long clientId) {
        this.clientId = clientId;
    }

    @Override
    public int hashCode(){
        final int prime = 31;
        int result = 1;
        result = prime * result + (((transactionDate == null)) ? 0 : transactionDate.hashCode());
        result = prime * result + ((clientId == null) ? 0 : clientId.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj){
        if (this == obj)
            return true;
        if(obj == null)
            return false;
        if(getClass() != obj.getClass())
            return false;
        Order other = (Order) obj;
        if (transactionDate == null){
            if(other.transactionDate != null)
                return false;
        } else if (!transactionDate.equals(other.transactionDate))
            return false;
        if (clientId == null) {
            if (other.clientId != null)
                return false;
        }else if(!clientId.equals((other.clientId)))
            return false;
        return true;
    }

    @Override
    public String toString(){return "Order [id=" + id + ", transaction date" + transactionDate + "]\n";}

}


