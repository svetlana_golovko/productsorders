package com.ordersystem.ee.persistence.repository;

import com.ordersystem.ee.persistence.model.Client;
import org.springframework.data.repository.CrudRepository;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

public interface IClientRepository extends CrudRepository<Client, Long> {
    List<Client> findByFirstName(String name);

    Optional<Client> findByLastName(String name);
    List<Client> findByDateCreatedBetween(LocalDate start, LocalDate end);
    List<Client> findByPhone(String phone);
}
