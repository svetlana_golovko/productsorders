package com.ordersystem.ee.persistence.repository;

import com.ordersystem.ee.persistence.model.Product;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

public interface IProductRepository extends PagingAndSortingRepository<Product, Long> {

    Optional<Product> findByName(String name);

    @Query("select t from Product t where t.name like %?1%")
    List<Product> findByNameMatches(String name);

    List<Product> findByRealizationDateBetween(LocalDate start, LocalDate end);
}
