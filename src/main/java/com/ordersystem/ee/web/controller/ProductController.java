package com.ordersystem.ee.web.controller;

import com.ordersystem.ee.persistence.model.Product;
import com.ordersystem.ee.service.IProductService;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping(value = "/products")
public class ProductController {
    private IProductService productService;

    public ProductController(IProductService productService) {
        super();
        this.productService = productService;
    }

    @GetMapping(path="/{id}")
    public Product findOne(@PathVariable Long id){
        return productService.findById(id).orElseThrow(()-> new ResponseStatusException(HttpStatus.NOT_FOUND));
    }

    @PostMapping()
    public Long create(@RequestBody Product product){
        Product savedProduct = this.productService.save(product);
        return savedProduct.getId();
    }

    @PutMapping(path="/{id}")
    public Long update(@PathVariable Long id, @RequestBody Product product) {
        this.productService.findById(id).orElseThrow(()-> new ResponseStatusException(HttpStatus.NOT_FOUND));
        product.setId(id);
        Product savedProduct = this.productService.save(product);
        return savedProduct.getId();
    }

    @DeleteMapping(path="/{id}")
    public void delete(@PathVariable Long id){
        this.productService.delete(id);
    }

    @GetMapping(path= "")
    public List<Product> findAll(@RequestParam Optional<String> sort, @RequestParam Optional<String> forward) {
        Boolean asc = forward.equals("Y") ? Boolean.TRUE : Boolean.FALSE;
        return productService.findAllAndSortBy(sort.orElseGet(() -> "name"), asc);
    }

}
