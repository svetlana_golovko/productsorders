package com.ordersystem.ee.persistence.repository;

import static org.hamcrest.CoreMatchers.hasItems;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.hasSize;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;

import java.time.LocalDate;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import com.ordersystem.ee.persistence.model.Product;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Order;

@SpringBootTest
public class ProductRepositoryIntegrationTest {

    @Autowired
    IProductRepository productRepository;

    @Test
    public void givenProjectCreated_whenFindByTaskNameMatches_thenSuccess() {
        Product product1 = new Product("Low Priority Product", 1.0, "Low Priority Product", LocalDate.now());
        Product product2 = new Product("Low Priority Product", 2.0, "Low Priority Product", LocalDate.now());
        Product product3 = new Product("High Priority Product", 3.0, "High Priority Product", LocalDate.now());
        Product product4 = new Product("High Priority Product", 4.0, "High Priority Product", LocalDate.now());

        productRepository.save(product1);
        productRepository.save(product2);
        productRepository.save(product3);
        productRepository.save(product4);

        List<Product> retrievedTasks = productRepository.findByNameMatches("High");
        assertThat(retrievedTasks, contains(product3, product4));
    }

    @Test
    public void givenClientCreated_whenFindByDateCreatedBetween_thenSuccess() {
        Product oldProduct = new Product("Product 1", 1.0, "Product", LocalDate.now().minusDays(5));
        productRepository.save(oldProduct);

        Product newProduct = new Product("Product 2", 1.0, "Product", LocalDate.now());
        productRepository.save(newProduct);

        Product newProduct2 = new Product("Product 3", 1.0, "Product", LocalDate.now());
        productRepository.save(newProduct2);

        List<Product> retrievedProducts = productRepository.findByRealizationDateBetween(LocalDate.now()
                        .minusDays(1),
                LocalDate.now()
                        .plusDays(1));
        assertThat(retrievedProducts, hasItems(newProduct, newProduct2));
    }

    @Test
    public void givenDataCreated_whenFindAllPaginated_thenSuccess() {
        Page<Product> retrievedProducts = productRepository.findAll(PageRequest.of(0, 2));
        assertThat(retrievedProducts.getContent(), hasSize(2));
    }

    @Test
    public void givenDataCreated_whenFindAllSorted_thenSuccess() {
        List<Product> retrievedProducts = (List<Product>) productRepository.findAll(Sort.by(Order.asc("name")));
        List<Product> sortedProducts = retrievedProducts.stream().collect(Collectors.toList());
        sortedProducts.sort(Comparator.comparing(Product::getName));
        assertEquals(sortedProducts, retrievedProducts);
    }

    @Test
    public void givenDataCreated_whenFindAllPaginatedAndSorted_thenSuccess() {
        Page<Product> retrievedProducts = productRepository.findAll(PageRequest.of(0, 2, Sort.by(Order.asc("name"))));

        assertThat(retrievedProducts.getContent(), hasSize(2));
    }
}