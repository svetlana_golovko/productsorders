package com.ordersystem.ee.persistence.repository;

import com.ordersystem.ee.persistence.model.Client;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

@SpringBootTest
public class ClientRepositoryIntegrationTest {

    @Autowired
    private IClientRepository clientRepository;

    @Test
    public void whenSavingNewClient_thenSuccess() {
        Client newClient = new Client("FirstName", LocalDate.now());
        assertNotNull(clientRepository.save(newClient));
    }

    @Test
    public void givenClient_whenFindById_thenSuccess() {
        Client newClient = new Client("FirstName1", LocalDate.now());
        clientRepository.save(newClient);
        Optional<Client> retrievedClient = clientRepository.findById(newClient.getId());
        assertEquals(retrievedClient.get(), newClient);
    }

    @Test
    public void givenClientCreated_whenFindByName_thenSuccess() {
        Client newClient = new Client("FirstName2", LocalDate.now());
        clientRepository.save(newClient);
        List<Client> retrievedClient = clientRepository.findByFirstName(newClient.getFirstName());
        assertEquals(retrievedClient.get(0), newClient);
    }
}