DROP TABLE IF EXISTS product;

CREATE TABLE product (
    barcode int AUTO_INCREMENT PRIMARY KEY,
    name VARCHAR(250) NOT NULL,
    price double NOT NULL,
    description VARCHAR(250) DEFAULT NULL,
    realization_date date NOT NULL
);